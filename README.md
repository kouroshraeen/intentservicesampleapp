# IntentService Sample App

An app that uses an IntentService to make a REST call to the Open States API in a background thread.
The REST call is made using the Retrofit library and retrieves the bills in the current session of
Kansas state legislature. The result is then passed to the main activity, using a ResultReceiver, to
be displayed in a RecyclerView.

## Project Configuration

You will need to provide your own openstates.org API key for the app to work. Open or create the gradle.properties file located in .gradle directory
on your local machine and add the following line:

OpenStatesAPIKey=YOUR_API_KEY

where you replace YOUR_API_KEY with your own key.

## Libraries

* [ButterKnife](https://github.com/JakeWharton/butterknife)
* [Retrofit](https://github.com/square/retrofit)