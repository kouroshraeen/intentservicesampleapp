package com.kouroshraeen.intentservicesampleapp.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.kouroshraeen.intentservicesampleapp.BuildConfig;
import com.kouroshraeen.intentservicesampleapp.api.ApiClient;
import com.kouroshraeen.intentservicesampleapp.api.OpenStatesApi;
import com.kouroshraeen.intentservicesampleapp.model.Bill;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;


public class QueryService extends IntentService {
    private static final String TAG = QueryService.class.getSimpleName();

    public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;

    public QueryService() {
        super("QueryService");
    }

    public QueryService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        ResultReceiver receiver = intent.getParcelableExtra("receiver");
        Bundle bundle = new Bundle();

        OpenStatesApi apiService = ApiClient.getApiClient();
        Call<List<Bill>> call = apiService.getBills("ks", "session", "", "created_at", BuildConfig.OPEN_STATES_API_KEY);
        String url = call.request().url().toString();
        Log.i(TAG, url);

        try {
            //receiver.send(STATUS_RUNNING, Bundle.EMPTY);
            ArrayList<Bill> bills = (ArrayList<Bill>) call.execute().body();
            bundle.putParcelableArrayList("results", bills);
            receiver.send(STATUS_FINISHED, bundle);
            Log.i(TAG, "Number of bills: " + bills.size());
        } catch (IOException e) {
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(STATUS_ERROR, bundle);
        }
    }
}
