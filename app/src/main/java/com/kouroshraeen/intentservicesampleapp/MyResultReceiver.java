package com.kouroshraeen.intentservicesampleapp;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;


public class MyResultReceiver extends ResultReceiver {
    public static final String TAG = MyResultReceiver.class.getSimpleName();
    private Receiver mReceiver;

    public MyResultReceiver(Handler handler) {
        super(handler);
    }

    public void setReceiver(Receiver receiver) {
        Log.i(TAG, "setReceiver() called!");
        mReceiver = receiver;
    }

    public interface Receiver {
        public void onReceiveResult(int resultCode, Bundle resultData);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (mReceiver != null) {
            Log.i(TAG, "onReceiveResult() called!");
            mReceiver.onReceiveResult(resultCode, resultData);
        }
    }
}
