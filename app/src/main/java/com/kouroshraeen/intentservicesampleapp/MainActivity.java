package com.kouroshraeen.intentservicesampleapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.kouroshraeen.intentservicesampleapp.adapter.BillsAdapter;
import com.kouroshraeen.intentservicesampleapp.model.Bill;
import com.kouroshraeen.intentservicesampleapp.service.QueryService;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MyResultReceiver.Receiver {
    public static final String TAG = MainActivity.class.getSimpleName();
    private MyResultReceiver mReceiver;
    private ArrayList<Bill> mBills;
    private BillsAdapter mAdapter;

    @BindView(R.id.bill_list)
    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setSubtitle("Bills in Kansas state legislature");
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        mReceiver = new MyResultReceiver(new Handler());
        mReceiver.setReceiver(this);

        Intent intent = new Intent(this, QueryService.class);
        intent.putExtra("receiver", mReceiver);
        startService(intent);

    }

    @Override
    protected void onPause() {
        mReceiver.setReceiver(null);
        super.onPause();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        Log.i(TAG, "onReceiveResult() called!");
        switch (resultCode) {
            case QueryService.STATUS_RUNNING:
                break;
            case QueryService.STATUS_FINISHED:
                mBills = resultData.getParcelableArrayList("results");
                updateUi();
                Log.i(TAG, "Number of bills: " + mBills.size());
                break;
            case QueryService.STATUS_ERROR:
                break;
        }
    }

    private void updateUi() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, R.drawable.divider));
        mAdapter = new BillsAdapter(this, mBills);
        mRecyclerView.setAdapter(mAdapter);
    }
}
